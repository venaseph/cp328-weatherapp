//
//  ViewController.swift
//  Weather
//
//  Created by Chris Peragine on 1/23/18.
//  Copyright © 2018 Chris Peragine. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private var hiTemp: UILabel?
    @IBOutlet private var loTemp: UILabel?
    @IBOutlet private var blurbPhrase: UILabel?
    
    private let url = "https://apidev.accuweather.com/forecasts/v1/daily/5day/335315?details=true&apikey=JoshErvineDemoKeySouthHillsAccuX"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let weather = GetAccuWX(url: url)
        weather.getForecast { (weather) in
            
            if let hiText: Double = weather.dailyForecasts?[0].temp?.maximum?.value {
                self.hiTemp?.text = String(format: "%.0f", hiText) + "°"
            }
            
            if let loText: Double = weather.dailyForecasts?[0].temp?.minimum?.value {
                self.loTemp?.text = String(format: "%.0f", loText) + "°"
            }
            
            self.blurbPhrase?.text = weather.headLines?.text
            return
        }
        
        
    }




}


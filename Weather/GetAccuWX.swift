//
//  GetAccuWX.swift
//  Weather
//
//  Created by Chris Peragine on 1/23/18.
//  Copyright © 2018 Chris Peragine. All rights reserved.
//

import Foundation
import Alamofire

class GetAccuWX {
    
    private let url: String
    
    init (url: String) {
        self.url = url
    }

    func getForecast(completion: @escaping (Weather) -> Void) {
        //Async request, get is implied

        Alamofire.request(url).responseJSON{ response in
            if let result = response.result.value {
                guard let data = try? JSONSerialization.data(withJSONObject: result, options: []) else { return }
                guard let weather = try? JSONDecoder().decode(Weather.self, from: data) else { return }

                completion(weather)
                
            }
        }

    }

}

struct Weather: Codable {
    let headLines: HeadLines?
    let dailyForecasts: [DailyForecasts]?
    
    enum CodingKeys: String, CodingKey {
        case headLines = "Headline"
        case dailyForecasts = "DailyForecasts"
    }
}


struct DailyForecasts: Codable {
    let date: String?  // should handle, if I have time
    let sun: Sun?
    let moon: Moon?
    let temp: TempType?
    let realFeelTemperature: TempType?
    let realFeelTemperatureShade: TempType?
    let hoursOfSun: Double?
    let degreeDaySummary: DegreeDaySummary?
    let day: DayNight?
    let night: DayNight?
    let sources: [String]?
    let mobileLink: String?
    let link: String?
    
    enum CodingKeys: String, CodingKey {
        case date = "Date"
        case sun = "Sun"
        case moon = "Moon"
        case temp = "Temperature"
        case realFeelTemperature = "RealFeelTemperature"
        case realFeelTemperatureShade = "RealFeelTemperatureShade"
        case hoursOfSun = "HoursOfSun"
        case degreeDaySummary = "DegreeDaySummary"
        case day = "Day"
        case night = "Night"
        case sources = "Sources"
        case mobileLink = "MobileLink"
        case link = "Link"
    }
    
}

struct DayNight: Codable {
    let icon: Int?
    let iconPhrase: String?
    let shortPhrase: String?
    let longPhrase: String?
    let precipitationProbability: Int?
    let thunderstormProbability: Int?
    let rainProbability: Int?
    let snowProbability: Int?
    let iceProbability: Int?
    let wind: WindAndGust?
    let windGust: WindAndGust?
    let totalLiquid: ValUnitType?
    let rain: ValUnitType?
    let snow: ValUnitType?
    let ice: ValUnitType?
    let hoursOfPrecip: Int?
    let hoursOfRain: Int?
    let hoursOfSnow: Int?
    let hoursOfIce: Int?
    let cloudCover: Int?
    
    enum CodingKeys: String, CodingKey {
        case icon = "Icon"
        case iconPhrase = "IconPhrase"
        case shortPhrase = "ShortPhrase"
        case longPhrase = "LongPhrase"
        case precipitationProbability = "PrecipitationProbabilit"
        case thunderstormProbability = "ThunderstormProbability"
        case rainProbability = "RainProbability"
        case snowProbability = "SnowProbability"
        case iceProbability = "IceProbability"
        case wind = "Wind"
        case windGust = "WindGust"
        case totalLiquid = "TotalLiquid"
        case rain = "Rain"
        case snow = "Snow"
        case ice = "Ice"
        case hoursOfPrecip = "HoursOfPrecipitation"
        case hoursOfRain = "HoursOfRain"
        case hoursOfSnow = "HoursOfSnow"
        case hoursOfIce = "HoursOfIce"
        case cloudCover = "CloudCover"
    }
    
}

struct WindAndGust: Codable {
    let speed: ValUnitType?
    let direction: DegLocEng?
    
    enum CodingKeys: String, CodingKey {
        case speed = "Speed"
        case direction = "Direction"
    }
}

struct DegLocEng: Codable {
    let degrees: Int?
    let localized: String?
    let english: String?
    
    enum CodingKeys: String, CodingKey {
        case degrees = "Degrees"
        case localized = "Localized"
        case english = "English"
    }
}

struct DegreeDaySummary: Codable {  // prob could nest this with ValUnitType but meh
    let heating: ValUnitType?
    let cooling: ValUnitType?
    
    enum CodingKeys: String, CodingKey {
        case heating = "Heating"
        case cooling = "Cooling"
    }
}

struct RealFeelTemperatureShade: Codable {
    let minimum: ValUnitType?
    let maximum: ValUnitType?
    
    enum CodingKeys: String, CodingKey {
        case minimum = "Minimum"
        case maximum = "Maximum"
    }
}

struct RealFeelTemperature: Codable {
    let minimum: ValUnitType?
    let maximum: ValUnitType?
    
    enum CodingKeys: String, CodingKey {
        case minimum = "Minimum"
        case maximum = "Maximum"
    }
}

struct TempType: Codable {
    let minimum: ValUnitType?
    let maximum: ValUnitType?
    
    enum CodingKeys: String, CodingKey {
        case minimum = "Minimum"
        case maximum = "Maximum"
    }
}

struct ValUnitType: Codable {
    let value: Double?
    let unit: String?
    let unitType: Int?
    
    enum CodingKeys: String, CodingKey {
        case value = "Value"
        case unit = "Unit"
        case unitType = "UnitType"
    }
    
}

struct Moon: Codable {
    let rise: String?
    let set: String?
    let phase: String?
    let age: Int?
    
    enum CodingKeys: String, CodingKey {
        case rise = "Rise"
        case set = "Set"
        case phase = "Phase"
        case age = "Age"
    }
}

struct Sun: Codable {
    let rise: String?
    let set: String?
    
    enum CodingKeys: String, CodingKey {
        case rise = "Rise"
        case set = "Set"
    }
}

struct HeadLines: Codable {
    
    let effectiveDate: String?
    let severity: Int?
    let text: String?
    let category: String?
    let endDate: String?
    let MobileLink: String?
    let link: String?
    
    enum CodingKeys: String, CodingKey {
        case effectiveDate = "EffectiveDate"
        case severity = "Severity"
        case text = "Text"
        case category = "Category"
        case endDate = "EndDate"
        case MobileLink = "MobileLink"
        case link = "Link"
    }
}
